---
layout: page
title: About
permalink: /about/
---

This is my blog. I write about engineering, books, art, and the little things I am currently studying.

The theme for this blog is a modified Jekyll theme. The blog is hosted on GitLab Pages. 

You can find the source code for the Jekyll new theme at:
{% include icon-github.html username="jglovier" %} /
[jekyll-new](https://github.com/jglovier/jekyll-new)

You can find the source code for Jekyll at
{% include icon-github.html username="jekyll" %} /
[jekyll](https://github.com/jekyll/jekyll)
